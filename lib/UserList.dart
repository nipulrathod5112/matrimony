import 'package:flutter/material.dart';
import 'package:matrimony_app/database.dart';
import 'package:matrimony_app/my_home_screen.dart';
import 'package:matrimony_app/user_model.dart';
import 'add_user.dart';

class UserList extends StatefulWidget {
  const UserList({super.key});

  @override
  State<UserList> createState() => _UserList();
}

class _UserList extends State<UserList> {
  MyDatabase db = MyDatabase();
  List<UserModel> localList = [];
  List<UserModel> searchList = [];
  bool isGetData = true;
  FocusNode myFocusNode = FocusNode();
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    db.copyPasteAssetFileToRoot().then((value) {
      db.getUserListFromTbl();
    });
  }

  @override
  Widget build(BuildContext context) {
    return (Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SafeArea(
          child: Stack(
            children: [
              Container(
                color: Colors.white,
                child: Container(),
              ),
              FutureBuilder<List<UserModel>>(
                  builder: (context, snapshot) {
                    if (snapshot != null && snapshot.hasData) {
                      if (isGetData) {
                        localList.addAll(snapshot.data!);
                        searchList.addAll(localList);
                      }
                      isGetData = false;
                      return Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          // Search Btn
                          Container(
                            margin: const EdgeInsets.all(10),
                            // width: 400,
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              gradient: const LinearGradient(
                                colors: [
                                  Color.fromARGB(255, 238, 201, 136),
                                  Color.fromARGB(255, 239, 186, 202)
                                ],
                                begin: Alignment.bottomLeft,
                                end: Alignment.topRight,
                              ),
                            ),
                            child: TextField(
                              // focusNode: myFocusNode,
                              decoration: InputDecoration(
                                labelText: "Search Users",
                                labelStyle: TextStyle(
                                    color: myFocusNode.hasFocus
                                        ? Colors.white
                                        : Colors.white),
                                prefixIcon: const Icon(
                                  Icons.search_rounded,
                                  color: Colors.white,
                                ),
                                border: InputBorder.none,
                              ),
                              controller: controller,
                              onChanged: (value) {
                                searchList.clear();
                                for (int i = 0; i < localList.length; i++) {
                                  if (localList[i]
                                      .Name
                                      .toLowerCase()
                                      .contains(value)) {
                                    searchList.add(localList[i]);
                                  }
                                }
                                setState(() {});
                              },
                            ),
                          ),

                          // card
                          Expanded(
                            child: ListView.builder(
                              padding: const EdgeInsets.all(5),
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(
                                      builder: (context) =>
                                          AddUser(model: searchList[index]),
                                    ));
                                  },
                                  child: Container(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                      ),
                                      color: Colors.white,
                                      elevation: 3,
                                      borderOnForeground: false,
                                      child: (Container(
                                        padding: const EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(55),
                                        ),
                                        child: Row(
                                          children: [
                                            Expanded(
                                                child: Stack(
                                              children: [
                                                //btns
                                                Container(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 13),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: [
                                                      // like btn
                                                      Container(
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            setState(() {
                                                              if (searchList[
                                                                          index]
                                                                      .Favourite_user ==
                                                                  1) {
                                                                searchList[
                                                                        index]
                                                                    .Favourite_user = 2;
                                                              } else if (searchList[
                                                                          index]
                                                                      .Favourite_user ==
                                                                  2) {
                                                                searchList[
                                                                        index]
                                                                    .Favourite_user = 1;
                                                              }
                                                              db.updateFavouriteUser(
                                                                  searchList[
                                                                          index]
                                                                      .id,
                                                                  searchList[
                                                                          index]
                                                                      .Favourite_user);
                                                            });
                                                          },
                                                          child: Icon(
                                                            searchList[index]
                                                                        .Favourite_user ==
                                                                    2
                                                                ? Icons.favorite_border_outlined
                                                                : Icons
                                                                    .favorite,
                                                            color: Color.fromARGB(255, 241, 125, 155),
                                                            size: 30,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),

                                                // Details
                                                Column(
                                                  children: [
                                                    //name
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              10),
                                                      alignment:
                                                          Alignment.topLeft,
                                                      child: Text(
                                                        searchList[index]
                                                            .Name
                                                            .toString(),
                                                        style: const TextStyle(
                                                          color:
                                                              Color.fromARGB(255, 241, 125, 155),
                                                          fontSize: 25,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),

                                                    // DOB
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              10),
                                                      alignment:
                                                          Alignment.topLeft,
                                                      child: Text(
                                                        'DOB : ${searchList[index].DOB.toString()}',
                                                        style: TextStyle(
                                                            color: Colors
                                                                .grey.shade500,
                                                            fontSize: 17),
                                                      ),
                                                    ),

                                                    // mobile no.
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              10),
                                                      alignment:
                                                          Alignment.topLeft,
                                                      child: Text(
                                                        'Mobile no: ${searchList[index].MobileNo.toString()}',
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            color: Colors
                                                                .grey.shade500),
                                                      ),
                                                    ),

                                                    // gender
                                                    Container(
                                                      margin:
                                                          EdgeInsets.all(10),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                              'Gender : ${searchList[index].Gender as int == 1 ? "Male" : searchList[index].Gender as int == 2 ? "Female" : "Other"}',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .grey
                                                                      .shade500,
                                                                  fontSize:
                                                                      15)),
                                                          const SizedBox(
                                                            width: 20,
                                                          ),
                                                        ],
                                                      ),
                                                    ),

                                                    // city
                                                    Container(
                                                      margin:
                                                      const EdgeInsets.all(
                                                          10),
                                                      alignment:
                                                      Alignment.topLeft,
                                                      child: Text(
                                                        'City: ${searchList[index].City_id as int == 1 ? "Ahmedabad" : searchList[index].City_id as int == 2 ? "Rajkot" : "Vadodara"}',
                                                        style: TextStyle(
                                                            fontSize: 15,
                                                            color: Colors
                                                                .grey.shade500),
                                                      ),
                                                    ),

                                                    // delete btn
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          child:
                                                              GestureDetector(
                                                            onTap: () {
                                                              showDialog(
                                                                  context:
                                                                      context,
                                                                  builder:
                                                                      (BuildContext
                                                                          contex) {
                                                                    return AlertDialog(
                                                                      title: Text(
                                                                          "Delete User"),
                                                                      content: Text(
                                                                          "Are you sure?"),
                                                                      actions: [
                                                                        Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.center,
                                                                          children: [
                                                                            ElevatedButton(
                                                                              onPressed: () async {
                                                                                int deletedUserID = await db.deleteUserFromUserTable(localList[index].id);
                                                                                if (deletedUserID > 0) {
                                                                                  localList.removeAt(index);
                                                                                }
                                                                                ;
                                                                                setState(() {
                                                                                  Navigator.push(
                                                                                    context,
                                                                                    MaterialPageRoute(builder: (context) => MyHomeScreen()),
                                                                                  );
                                                                                });
                                                                              },
                                                                              child: Text("Delete"),
                                                                            ),
                                                                            SizedBox(
                                                                              width: 5,
                                                                            ),
                                                                            ElevatedButton(
                                                                              onPressed: () {
                                                                                Navigator.of(context).pop();
                                                                              },
                                                                              child: Text("No"),
                                                                            )
                                                                          ],
                                                                        )
                                                                      ],
                                                                    );
                                                                  });
                                                            },
                                                            child: Expanded(
                                                              child: Container(
                                                                // height: 20,
                                                                padding: EdgeInsets
                                                                    .symmetric(
                                                                  vertical: 10,
                                                                  horizontal:
                                                                      15,
                                                                ),
                                                                // width: 20,
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        bottom:
                                                                            15),
                                                                decoration: BoxDecoration(
                                                                    color: Color
                                                                        .fromARGB(
                                                                            255, 241, 125, 155),
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            10)),
                                                                child: Text(
                                                                    "Delete",style: TextStyle(color: Colors.white),),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            )),
                                            Icon(
                                              Icons
                                                  .keyboard_arrow_right_outlined,
                                              color: Colors.grey.shade400,
                                              size: 24,
                                            ),
                                          ],
                                        ),
                                      )),
                                    ),
                                  ),
                                );
                              },
                              itemCount: searchList.length,
                            ),
                          ),
                        ],
                      );
                    } else {
                      return (const Center(
                        child: Text('No User Found'),
                      ));
                    }
                  },
                  future: isGetData ? db.getUserListFromTbl() : null),
            ],
          ),
        ),
      ),
    ));
  }

  showAlertDialog(BuildContext context, index) {
    // set up the button
    Widget YesButton = TextButton(
      child: Text("Yes"),
      onPressed: () async {
        int deletedUserID =
            await db.deleteUserFromUserTable(localList[index].id);
        if (deletedUserID > 0) {
          localList.removeAt(index);
        }
        Navigator.of(context, rootNavigator: true).pop('dialog');
        setState(() {});
      },
    );

    Widget NoButton = TextButton(
      child: Text("No"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Are You Sure You Want to Delete."),
      actions: [YesButton, NoButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

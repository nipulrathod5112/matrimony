import 'package:flutter/material.dart';
import 'package:matrimony_app/my_home_screen.dart';
import 'package:matrimony_app/onboarding/onboarding.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _navigatetohome();
  }

  _navigatetohome()async{
    await Future.delayed(const Duration(milliseconds: 1500),(){});
    // ignore: use_build_context_synchronously
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const OnBoarding()));
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Image(
              height: 100,
              width: 100,
              image: AssetImage('assets/logomatrimony.png'),),
            Text(
              'Matrimony',
                  style: TextStyle(fontSize: 24,fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}

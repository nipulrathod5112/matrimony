import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:matrimony_app/UserList.dart';
import 'package:matrimony_app/navbar.dart';

class MyHomeScreen extends StatefulWidget {
  const MyHomeScreen({Key? key}) : super(key: key);

  @override
  State<MyHomeScreen> createState() => _MyHomeScreenState();
}

class _MyHomeScreenState extends State<MyHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Container(
        child: const NavDrawer(),
      ),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Builder(
          builder: (context) => IconButton(
            icon: const Icon(
              CupertinoIcons.line_horizontal_3,
              color: Colors.black,
            ),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        centerTitle: true,
        title: Text(
          "Matrimony",
        ),
        actions: [
          GestureDetector(
            onTap: () {},
            child: Container(
              padding: const EdgeInsets.only(right: 15),
              child: const Icon(
                Icons.account_circle_sharp,
                color: Colors.black,
                size: 30,
              ),
            ),
          ),
        ],
      ),
      body: UserList(),
    );
  }
}

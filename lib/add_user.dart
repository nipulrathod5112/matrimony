import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:matrimony_app/city_model.dart';
import 'package:matrimony_app/database.dart';
import 'package:matrimony_app/gender_model.dart';
import 'package:matrimony_app/my_home_screen.dart';
import 'package:matrimony_app/user_model.dart';
import 'package:intl/intl.dart';
import 'navbar.dart';

class AddUser extends StatefulWidget {
  late UserModel? model = UserModel();

  AddUser({super.key, required this.model});

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  late CityModel model = CityModel(CityName1: 'Select City', CityID1: -1);
  late GenderModel model1 =
      GenderModel(genderID1: -1, genderName1: 'Select Gender');

  bool isGetCity = true;
  bool isGetGender = true;

  DateTime selectedDate = DateTime.now();
  final _formKey = GlobalKey<FormState>();
  MyDatabase myDatabase = MyDatabase();
  TextEditingController nameController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController heightController = TextEditingController();

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(
        text: widget.model != null ? widget.model!.Name.toString() : '');
    mobileController = TextEditingController(
        text: widget.model != null ? widget.model!.MobileNo.toString() : '');
  }

  CityModel? _ddSelectedValue;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: (Scaffold(
          drawer: Container(
            width: 222,
            child: const NavDrawer(),
          ),
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: Builder(
              builder: (context) => IconButton(
                icon: const Icon(
                  CupertinoIcons.line_horizontal_3,
                  color: Colors.black,
                ),
                onPressed: () => Scaffold.of(context).openDrawer(),
              ),
            ),
            centerTitle: true,
            title: Text(
              "Matrimony",
            ),
            actions: [
              GestureDetector(
                onTap: () {},
                child: Container(
                  padding: const EdgeInsets.only(right: 15),
                  child: const Icon(
                    Icons.account_circle_sharp,
                    color: Colors.black,
                    size: 30,
                  ),
                ),
              ),
            ],
          ),
          body: Stack(
            children: [
              Container(
                decoration: BoxDecoration(color: Colors.white),
                child: Container(),
              ),
              //singlechildscrollview
              GestureDetector(
                onTap: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                },
                child: Container(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        // Name
                        Container(
                          margin: const EdgeInsets.only(top: 15),
                          child: Row(
                            children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  child: const Icon(
                                    Icons.person,
                                    size: 30,
                                    color: Color.fromARGB(255, 241, 125, 155),
                                  )),
                              Container(
                                width: 300,
                                child: TextFormField(
                                    controller: nameController,
                                    validator: (value) {
                                      if (value == null ||
                                          value.trim().length == 0) {
                                        return 'Name is Empty ';
                                      } else {
                                        return null;
                                      }
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Enter Your Name",
                                      labelStyle: const TextStyle(
                                          color: Colors.black38,
                                          fontWeight: FontWeight.bold),
                                      hintText: "Nipul Rathod",
                                      hintStyle: const TextStyle(
                                          color: Colors.black12,
                                          fontWeight: FontWeight.bold),
                                      border: myinputborder(),
                                      //normal border
                                      enabledBorder: myinputborder(),
                                      //enabled border
                                      focusedBorder:
                                          myfocusborder(), //focused border
                                      // set more border style like disabledBorder
                                    )),
                              ),
                            ],
                          ),
                        ),

                        //Mobile Number
                        Container(
                          margin: const EdgeInsets.only(top: 20),
                          child: Row(
                            children: [
                              Container(
                                  margin: const EdgeInsets.all(10),
                                  child: const Icon(
                                    Icons.phone,
                                    size: 30,
                                    color: Color.fromARGB(255, 241, 125, 155),
                                  )),
                              Container(
                                width: 300,
                                child: TextFormField(
                                    controller: mobileController,
                                    validator: (value) {
                                      if (value == null ||
                                          value.trim().length == 0 ||
                                          value.trim().length > 10) {
                                        return 'Enter Correct Mobile Number ';
                                      } else {
                                        return null;
                                      }
                                    },
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                      labelText: "Enter Mobile Number",
                                      labelStyle: const TextStyle(
                                          color: Colors.black38,
                                          fontWeight: FontWeight.bold),
                                      hintText: "+91 9876543210",
                                      hintStyle: const TextStyle(
                                          color: Colors.black12,
                                          fontWeight: FontWeight.bold),
                                      border: myinputborder(),
                                      //normal border
                                      enabledBorder: myinputborder(),
                                      //enabled border
                                      focusedBorder:
                                          myfocusborder(), //focused border
                                      // set more border style like disabledBorder
                                    )),
                              ),
                            ],
                          ),
                        ),

                        // Birthdate
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 20),
                          child: Row(
                            children: [
                              const Text(
                                'Select Your Birthday : ',
                                style: TextStyle(
                                    color: Color.fromARGB(255, 241, 125, 155),
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                              InkWell(
                                  child: Container(
                                      width: 130,
                                      height: 50,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(18),
                                        gradient: LinearGradient(
                                          colors: [
                                            Color.fromARGB(255, 218, 178, 93),
                                            Color.fromARGB(255, 241, 125, 155)
                                          ],
                                          begin: Alignment.bottomLeft,
                                          end: Alignment.topRight,
                                        ),
                                      ),
                                      child: Text(
                                        getFormattedDateTime(selectedDate),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  onTap: () {
                                    _pickDateDialog();
                                  }),
                            ],
                          ),
                        ),

                        // Gender & city
                        Row(
                          children: [
                            //city
                            Container(
                              padding: EdgeInsets.only(top: 10),
                              // width: double.infinity,
                              child: FutureBuilder<List<CityModel>>(
                                builder: (context, snapshot) {
                                  if (snapshot != null && snapshot.hasData) {
                                    if (isGetCity) {
                                      // model = snapshot.data![0];
                                      _ddSelectedValue = snapshot.data![0];
                                      isGetCity=false;
                                    }

                                    return Container(
                                      padding: EdgeInsets.all(10),
                                      child: DropdownButtonHideUnderline(
                                          child: DecoratedBox(
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              colors: [
                                                Color.fromARGB(
                                                    255, 218, 178, 93),
                                                Color.fromARGB(
                                                    255, 241, 125, 155)
                                              ],
                                              begin: Alignment.bottomLeft,
                                              end: Alignment.topRight,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(18),
                                            boxShadow: const <BoxShadow>[
                                              BoxShadow(
                                                  color: Color.fromARGB(
                                                      255, 218, 178, 93),
                                                  //shadow for button
                                                  blurRadius: 10)
                                              //blur radius of shadow
                                            ]),
                                        child: DropdownButton2(
                                          value: _ddSelectedValue,
                                          items: snapshot.data!
                                              .map(
                                                (CityModel item) => DropdownMenuItem<
                                                    CityModel?>(
                                                  value: item,
                                                  child: Text(
                                                    item.CityName.toString(),
                                                    style: const TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white,
                                                    ),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                              )
                                              .toList(),
                                          onChanged: (value) {
                                            // isGetCity = false;
                                            setState(() {
                                              // model = value!;
                                              _ddSelectedValue=value;
                                            });
                                          },
                                          icon: Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: const Icon(
                                              Icons.keyboard_arrow_down_rounded,
                                              color: Colors.white,
                                            ),
                                          ),
                                          buttonDecoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(14),
                                          ),
                                          dropdownDecoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(24),
                                              color: Color.fromARGB(
                                                  255, 241, 125, 155)),
                                        ),
                                      )),
                                    );
                                  } else {
                                    return Container();
                                  }
                                },
                                future: isGetCity
                                    ? MyDatabase().getCityListFromTbl()
                                    : null,
                              ),
                            ),

                            //gender
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              width: 175,
                              child: FutureBuilder<List<GenderModel>>(
                                builder: (context, snapshot) {
                                  if (snapshot != null && snapshot.hasData) {
                                    if (isGetGender) {
                                      model1 = snapshot.data![0];
                                    }
                                    return Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: DropdownButtonHideUnderline(
                                          child: DecoratedBox(
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              colors: [
                                                Color.fromARGB(
                                                    255, 241, 125, 155),
                                                Color.fromARGB(
                                                    255, 218, 178, 93)
                                              ],
                                              begin: Alignment.bottomLeft,
                                              end: Alignment.topRight,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(18),
                                            boxShadow: const <BoxShadow>[
                                              BoxShadow(
                                                  color: Color.fromARGB(
                                                      255, 218, 178, 93),
                                                  //shadow for button
                                                  blurRadius: 10)
                                              //blur radius of shadow
                                            ]),
                                        child: DropdownButton2(
                                          items: snapshot.data!
                                              .map((items) => DropdownMenuItem<
                                                      GenderModel?>(
                                                    value: items,
                                                    child: Text(
                                                      items.genderName
                                                          .toString(),
                                                      style: const TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white,
                                                      ),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                  ))
                                              .toList(),
                                          value: model1,
                                          onChanged: (value) {
                                            isGetGender = false;
                                            setState(() {
                                              model1 = value!;
                                            });
                                          },
                                          icon: Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: const Icon(
                                              Icons.keyboard_arrow_down_rounded,
                                              color: Colors.white,
                                            ),
                                          ),
                                          buttonDecoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(14),
                                          ),
                                          dropdownDecoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(24),
                                              color: Color.fromARGB(
                                                  255, 241, 125, 155)),
                                        ),
                                      )),
                                    );
                                  } else {
                                    return Container();
                                  }
                                },
                                future: isGetGender
                                    ? myDatabase.getGenderFromTbl()
                                    : null,
                              ),
                            ),
                          ],
                        ),

                        //submit btn
                        Container(
                          margin: EdgeInsets.only(
                            top: 20,
                            bottom: 30,
                          ),
                          child: ElevatedButton(
                            onPressed: () {
                              print(model.City_id);
                              if (_formKey.currentState!.validate()) {
                                if (model.City_id == -1) {
                                  showAlertDialog(context);
                                } else {
                                  setState(() {
                                    myDatabase.upsertIntoUserTable(
                                        cityId: model.City_id,
                                        userName:
                                            nameController.text.toString(),
                                        dob: selectedDate.toString(),
                                        gender: model1.genderID,
                                        mobileNo:
                                            mobileController.text.toString(),
                                        id: widget.model != null
                                            ? widget.model!.id
                                            : -1);
                                  });
                                }
                              }
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => MyHomeScreen(),
                                ),
                              );
                            },
                            style: ElevatedButton.styleFrom(
                                shadowColor: Color.fromARGB(255, 218, 178, 93),
                                elevation: 10,
                                backgroundColor:
                                    Color.fromARGB(255, 241, 125, 155),
                                fixedSize: const Size(150, 50),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50))),
                            child: const Text(
                              'Submit',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        //submit
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ))),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Please Select City."),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAlertDialog2(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Age is below 18 years."),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _pickDateDialog() {
    showDatePicker(
            context: context,
            initialDate: selectedDate,
            firstDate: DateTime(1950),
            lastDate: DateTime.now())
        .then((pickedDate) {
      if (DateTime.now().difference(pickedDate!) < Duration(days: 6570) ||
          pickedDate == null) {
        return showAlertDialog2(context);
      }
      setState(() {
        selectedDate = pickedDate;
      });
    });
  }

  String getFormattedDateTime(dateToFormat) {
    DateFormat formatter = DateFormat.yMMMd();
    final String formatted = formatter.format(dateToFormat);
    if (dateToFormat != null) {
      return formatted;
    } else {
      return DateFormat('dd-MM-yyyy').format(DateTime.now());
    }
  }

  OutlineInputBorder myinputborder() {
    //return type is OutlineInputBorder
    return OutlineInputBorder(
        //Outline border type for TextField
        borderRadius: BorderRadius.all(Radius.circular(20)),
        borderSide: BorderSide(
          color: Color.fromARGB(255, 241, 125, 155),
          width: 3,
        ));
  }

  OutlineInputBorder myfocusborder() {
    return OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        borderSide: BorderSide(
          color: Color.fromARGB(255, 241, 125, 155),
          width: 3,
        ));
  }
}

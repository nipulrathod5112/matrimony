import 'dart:io';

import 'package:flutter/services.dart';
import 'package:matrimony_app/city_model.dart';
import 'package:matrimony_app/gender_model.dart';
import 'package:matrimony_app/user_model.dart';
// import 'package:matrimony/Database/religion_model.dart';
// import 'package:matrimony/Database/user_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class MyDatabase {
  Future<Database> initDatabase() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String databasePath = join(appDocDir.path, 'matrimony.db');
    return await openDatabase(
      databasePath,
      version: 2,
    );
  }

  Future<void> copyPasteAssetFileToRoot() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "matrimony.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data =
      await rootBundle.load(join('assets/database/', 'matrimony.db'));
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes);
    }
  }

  Future<int> deleteUserFromUserTable(id)
  async {
    Database db = await initDatabase();
    int deletedid = await db.delete('user_table',where: 'id = ?',whereArgs: [id]);
    return deletedid;
  }
  Future<void> updateFavouriteUser(int id, favUser)
  async {
    Database db = await initDatabase();
    Map<String, Object?> map = Map();
    map['Favourite_user'] = favUser;
    if(id != -1)
    {
      await db.update('user_table', map,where: 'id = ?',whereArgs: [id]);
    }else{
      await db.insert('user_table', map);
    }

  }
  Future<void> upsertIntoUserTable({cityId,userName,dob,id,gender,mobileNo})
  async {
    Database db = await initDatabase();
    Map<String, Object?> map = Map();
    map['Name']=userName;
    map['DOB']=dob;
    map['city_id']=cityId;
    map['Gender']=gender;
    map['MobileNo']=mobileNo;
    map['Favourite_user']=2;
    if(id != -1)
    {
      await db.update('user_table', map,where: 'id = ?',whereArgs: [id]);
    }else{
      await db.insert('user_table', map);
    }

  }
  Future<List<CityModel>> getCityListFromTbl() async {
    List<CityModel> cityList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
    await db.rawQuery('select * from city_table');
    CityModel model = CityModel(CityName1: 'Select City',CityID1: -1);
    model.City_id = -1;
    model.CityName1='Select City';
    cityList.add(model);
    for (int i = 0; i < data.length; i++) {
      model = CityModel(
          CityID1: data[i]['city_id'] as int,
          CityName1: data[i]['city_name'].toString());
      cityList.add(model);
    }
    return cityList;
  }
  Future<List<GenderModel>> getGenderFromTbl() async {
    List<GenderModel> genderList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
    await db.rawQuery('select * from gender_table');
    GenderModel model = GenderModel(genderName1: 'Select Gender',genderID1: -1);
    model.genderID = -1;
    model.genderName='Select Gender';
    genderList.add(model);
    for (int i = 0; i < data.length; i++) {
      model = GenderModel(
          genderID1: data[i]['gender_id'] as int,
          genderName1: data[i]['gender'].toString());
      genderList.add(model);
    }
    return genderList;
  }

  Future<List<UserModel>> getUserListFromTbl() async {
    List<UserModel> userList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
    await db.rawQuery('select * from user_table');
    for(int i=0; i<data.length; i++)
    {
      UserModel model = UserModel();
      model.City_id = data[i]['City_id'] as int;
      model.Name = data[i]['Name'].toString();
      model.id = data[i]['id'] as int;
      model.DOB = data[i]['DOB'].toString();
      model.Gender =  data[i]['Gender'] as int;
      model.MobileNo = data[i]['MobileNo'] as int;
      model.Favourite_user = data[i]['Favourite_user'] as int;
      userList.add(model);
    }
    return userList;
  }

  Future<List<UserModel>> getFavouriteUserFromTbl() async {
    List<UserModel> favUserList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
    await db.rawQuery('select * from user_table where Favourite_user = 1');
    for(int i=0; i<data.length; i++)
    {
      UserModel model = UserModel();
      model.City_id = data[i]['City_id'] as int;
      model.Name = data[i]['Name'].toString();
      model.id = data[i]['id'] as int;
      model.DOB = data[i]['DOB'].toString();
      model.Gender =  data[i]['Gender'] as int;
      model.MobileNo = data[i]['MobileNo'] as int;
      favUserList.add(model);
    }
    return favUserList;
  }
}

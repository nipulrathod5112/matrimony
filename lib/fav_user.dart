import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony_app/database.dart';
import 'add_user.dart';
import 'gender_model.dart';
import 'navbar.dart';
import 'user_model.dart';

class Favouriteuser extends StatefulWidget {
  @override
  State<Favouriteuser> createState() => _FavouriteuserState();
}

class _FavouriteuserState extends State<Favouriteuser> {
  MyDatabase db = MyDatabase();
  List<UserModel> localList = [];
  List<UserModel> searchList = [];
  List<GenderModel> genderList = [];
  bool isGetData = true;
  FocusNode myFocusNode = new FocusNode();
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    db.copyPasteAssetFileToRoot().then((value) {
      db.getUserListFromTbl();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Container(
        width: 222,
        child: const NavDrawer(),
      ),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Builder(
          builder: (context) => IconButton(
            icon: const Icon(
              CupertinoIcons.line_horizontal_3,
              color: Colors.black,
            ),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        centerTitle: true,
        title: Text(
          "Matrimony",
        ),
        actions: [
          GestureDetector(
            onTap: () {},
            child: Container(
              padding: const EdgeInsets.only(right: 15),
              child: const Icon(
                Icons.account_circle_sharp,
                color: Colors.black,
                size: 30,
              ),
            ),
          ),
        ],
      ),
      body: SafeArea(
          child: Stack(
        children: [
          Container(
            color: Colors.red.shade50,
            child: Container(),
          ),
          FutureBuilder<List<UserModel>>(
              builder: (context, snapshot) {
                if (snapshot != null && snapshot.hasData) {
                  if (isGetData) {
                    localList.addAll(snapshot.data!);
                    searchList.addAll(localList);
                  }
                  isGetData = false;
                  return Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      // Search btn
                      Container(
                        margin: const EdgeInsets.all(10),
                        // width: 400,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          gradient: const LinearGradient(
                            colors: [
                              Color.fromARGB(255, 238, 201, 136),
                              Color.fromARGB(255, 239, 186, 202)
                            ],
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                          ),
                        ),
                        child: TextField(
                          // focusNode: myFocusNode,
                          decoration: InputDecoration(
                            labelText: "Search Users",
                            labelStyle: TextStyle(
                                color: myFocusNode.hasFocus
                                    ? Colors.white
                                    : Colors.white),
                            prefixIcon: const Icon(
                              Icons.search_rounded,
                              color: Colors.white,
                            ),
                            border: InputBorder.none,
                          ),
                          controller: controller,
                          onChanged: (value) {
                            searchList.clear();
                            for (int i = 0; i < localList.length; i++) {
                              if (localList[i]
                                  .Name
                                  .toLowerCase()
                                  .contains(value)) {
                                searchList.add(localList[i]);
                              }
                            }
                            setState(() {});
                          },
                        ),
                      ),

                      //card
                      Expanded(
                        child: ListView.builder(
                          padding: EdgeInsets.all(5),
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      AddUser(model: searchList[index]),
                                ));
                              },
                              child: Container(
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  elevation: 0,
                                  color: Colors.white,
                                  borderOnForeground: true,
                                  child: (Container(
                                    padding: EdgeInsets.all(5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            children: [
                                              //name
                                              Container(
                                                margin:
                                                    const EdgeInsets.all(10),
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  searchList[index]
                                                      .Name
                                                      .toString(),
                                                  style: const TextStyle(
                                                    color: Color.fromARGB(
                                                        255, 241, 125, 155),
                                                    fontSize: 25,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),

                                              // DOB
                                              Container(
                                                margin:
                                                    const EdgeInsets.all(10),
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  'DOB : ${searchList[index].DOB.toString()}',
                                                  style: TextStyle(
                                                      color:
                                                          Colors.grey.shade500,
                                                      fontSize: 17),
                                                ),
                                              ),

                                              // mobile no.
                                              Container(
                                                margin:
                                                    const EdgeInsets.all(10),
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  'Mobile no: ${searchList[index].MobileNo.toString()}',
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color:
                                                          Colors.grey.shade500),
                                                ),
                                              ),

                                              // gender
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      'Gender : ${searchList[index].Gender as int == 1 ? "Male" : searchList[index].Gender as int == 2 ? "Female" : "Other"}',
                                                      style: TextStyle(
                                                          color: Colors
                                                              .grey.shade500,
                                                          fontSize: 15),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              //city
                                              Container(
                                                margin: EdgeInsets.all(10),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      'City: ${searchList[index].City_id as int == 1 ? "Ahmedabad" : searchList[index].City_id as int == 2 ? "Rajkot" : "Vadodara"}',
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          color: Colors
                                                              .grey.shade500),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Icon(
                                          Icons.keyboard_arrow_right_outlined,
                                          color: Colors.grey.shade400,
                                          size: 24,
                                        ),
                                      ],
                                    ),
                                  )),
                                ),
                              ),
                            );
                          },
                          itemCount: searchList.length,
                        ),
                      ),
                    ],
                  );
                } else {
                  return (Center(
                    child: Text('No User Found'),
                  ));
                }
              },
              future: isGetData ? db.getFavouriteUserFromTbl() : null),
        ],
      )),
    );
  }

  showAlertDialog(BuildContext context, index) {
    // set up the button
    Widget YesButton = TextButton(
      child: Text("Yes"),
      onPressed: () async {
        int deletedUserID =
            await db.deleteUserFromUserTable(localList[index].id);
        if (deletedUserID > 0) {
          localList.removeAt(index);
        }
        Navigator.of(context, rootNavigator: true).pop('dialog');
        setState(() {});
      },
    );

    Widget NoButton = TextButton(
      child: Text("No"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Are You Sure You Want to Delete."),
      actions: [YesButton, NoButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  OutlineInputBorder myinputborder() {
    //return type is OutlineInputBorder
    return OutlineInputBorder(
        //Outline border type for TextFeild
        borderRadius: BorderRadius.all(Radius.circular(20)),
        borderSide: BorderSide(
          color: Color.fromARGB(255, 241, 125, 155),
          width: 3,
        ));
  }
}

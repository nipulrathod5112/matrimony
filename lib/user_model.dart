class UserModel{
  late int _id;
  late String _Name;
  late String _DOB;
  late int _Gender;
  late int _MobileNo;
  // late String _Height;
  late int _City_id;
  late int _Favourite_user;

  int get Favourite_user => _Favourite_user;

  set Favourite_user(int FavouriteUser) {
    _Favourite_user = FavouriteUser;
  }

  int get id => _id;

  set id(int UserID) {
    _id = UserID;
  }

  String get Name => _Name;

  set Name(String UserName) {
    _Name = UserName;
  }


  String get DOB => _DOB;

  set DOB(String DOB) {
    _DOB = DOB;
  }


  int get Gender => _Gender;

  set Gender(int Gender) {
    _Gender = Gender;
  }


  int get MobileNo => _MobileNo;

  set MobileNo(int MobileNo) {
    _MobileNo = MobileNo;
  }



  int get City_id => _City_id;

  set City_id(int CityID) {
    _City_id = CityID;
  }
}

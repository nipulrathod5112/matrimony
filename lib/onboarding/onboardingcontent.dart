class OnBoardingContent{
  String image;
  String title;
  String discription;

  OnBoardingContent({required this.image,required this.title,required this.discription});
}

List<OnBoardingContent> contents = [
  OnBoardingContent(
    title: "Find your life partner",
    image: "assets/Matrimony_Vector_1.png",
    discription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra suspendisse potenti nullam ac.",
  ),
  OnBoardingContent(
    title: "Happy Life",
    image: "assets/Matrimony_Vector_2.png",
    discription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra suspendisse potenti nullam ac.",
  ),
  OnBoardingContent(
    title: "Perfect life partner",
    image: "assets/Matrimony_Vector_3.png",
    discription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra suspendisse potenti nullam ac.",
  ),
];
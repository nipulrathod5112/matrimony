import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony_app/add_user.dart';
import 'package:matrimony_app/fav_user.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 218, 178, 93),
                  Color.fromARGB(255, 241, 125, 155)
                ],
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: 32,
                  child: Text(
                    "N",
                    style: TextStyle(
                        fontSize: 35,
                        color: Color.fromARGB(255, 241, 125, 155),
                        fontWeight: FontWeight.w600),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Text(
                    "Matrimony",
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 30,
                      color: Colors.black,
                    ),
                  ),
                ),
                Container(
                  child: Text(
                    "Hello, Nipul",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Center(
            child: Container(
              //margin: EdgeInsets.only(top: 50,left: 60),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ListTile(
                    leading: const Icon(
                      Icons.home,
                      color: Color.fromARGB(255, 241, 125, 155),
                    ),
                    title: const Text(
                      'Home',
                      style: TextStyle(
                        fontFamily: 'Poppins-Bold',
                        fontWeight: FontWeight.w500,
                        letterSpacing: 0,
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                    onTap: () => {Navigator.of(context).pop()},
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.person,
                      color: Color.fromARGB(255, 241, 125, 155),
                    ),
                    title: const Text(
                      'Profile',
                      style: TextStyle(
                        fontFamily: 'Poppins-Bold',
                        fontWeight: FontWeight.w500,
                        letterSpacing: 0,
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                    onTap: () => {Navigator.of(context).pop()},
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.add,
                      color: Color.fromARGB(255, 241, 125, 155),
                    ),
                    title: const Text(
                      'Add User',
                      style: TextStyle(
                        fontFamily: 'Poppins-Bold',
                        fontWeight: FontWeight.w500,
                        letterSpacing: 0,
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                    onTap: () => {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => AddUser(model: null),
                        ),
                      ),
                    },
                  ),
                  ListTile(
                    leading: const Icon(
                      CupertinoIcons.heart,
                      color: Color.fromARGB(255, 241, 125, 155),
                    ),
                    title: const Text(
                      'Favourites',
                      style: TextStyle(
                        fontFamily: 'Poppins-Bold',
                        fontWeight: FontWeight.w500,
                        letterSpacing: 0,
                        fontSize: 18,
                        color: Colors.black,
                      ),
                    ),
                    onTap: () => {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Favouriteuser()))
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
